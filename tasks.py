from __future__ import absolute_import
from .celery import app
import time
# Selenium Codes
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException


@app.task
def login(user, pwd, driver):

    driver.switch_to.frame("mainFrame")

    username = driver.find_element_by_id("userid_sebenarnya")
    username.clear()
    username.send_keys(user)

    password = driver.find_element_by_id("pwd_sebenarnya")
    password.clear()
    password.send_keys(pwd)
    
    driver.find_element_by_id("btnSubmit").click()

    driver.switch_to.default_content()



@app.task()
def acc_details(driver, acc_id, acc_bal):
#    while True:
    try:
        driver.switch_to.frame("mainFrame")
        WebDriverWait(driver, 10).until(lambda driver: driver.find_element_by_xpath('//*[@id="col-main"]/div[2]/div[2]'))
        print(driver.page_source)
    

        acc_id = driver.find_element_by_css_selector("div.acc-left")
        acc_id = acc_id.find_element_by_css_selector("p").text

        acc_bal = driver.find_element_by_css_selector("p.nominal").text

        # Transaction History
        dropdown = driver.find_element_by_css_selector("div.dropdown").click()
        driver.find_element_by_xpath('//*[@id="col-main"]/div[2]/div[2]/div/ul/li[1]').click()
        print("DOne")
#        return {"Status": True}

    except TimeoutException:
        print("Oh no")
#        return {"Status": False}


@app.task()
def trans_info(driver, beg_bal, tot_deb, tot_cred, end_bal):
    try:
        WebDriverWait(driver, 10).until(lambda driver: driver.find_element_by_xpath('//*[@id="beginingBalance"]'))

        print(driver.page_source)
        beg_bal = driver.find_element_by_id("beginingBalance").text
        print(beg_bal)
        tot_deb = driver.find_element_by_id("totalDebits").text
        print(tot_deb)
        tot_cred = driver.find_element_by_id("totalCredits").text
        print(tot_cred)
        end_bal = driver.find_element_by_id("endingBalance").text
        print(end_bal)

        return {"Status": True}
#        acc_df.loc[acc_df.shape[0]] = [acc_id, acc_bal, beg_bal, tot_deb, tot_cred, end_bal]

    except TimeoutException:
        print("Timed out")
        return {"Status": False}
        

@app.task()
def site2df(acc_df, acc_id, acc_bal, beg_bal, tot_deb, tot_cred, end_bal):
    acc_df.loc[acc_df.shape[0]] = [acc_id, acc_bal, beg_bal, tot_deb, tot_cred, end_bal]
    return {"Status": True}