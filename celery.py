from __future__ import absolute_import
from celery import Celery

app = Celery('Mandiri_Scraper',
             broker ='localhost',
             backend ='rpc://', # pyamqp, amqp, 
             include = ['Mandiri_Scraper.tasks']
            )