# https://www.linode.com/docs/guides/task-queue-celery-rabbitmq/


from selenium.webdriver.chrome.options import Options
from selenium import webdriver
import pandas as pd
from .tasks import login, acc_details, trans_info, site2df
import time


def main():
    options = Options()
    options.page_load_strategy = 'normal'

    driver = webdriver.Chrome(executable_path='C:/Users/tonar/Documents/_Brick_/Mandiri_Scraper/chromedriver.exe', options=options)

    link = 'https://ibank.bankmandiri.co.id/retail3/'

    driver.get(link)

    user = "brickmandiri01"
    pwd = "BrickNew2021"

    acc_id = None
    acc_bal = None
    beg_bal = None
    tot_deb = None
    tot_cred = None
    end_bal = None

    acc_df = pd.DataFrame(columns = ['acc_id', 'acc_bal', 'beg_bal', 'tot_deb',
                                          'tot_cred', 'end_bal'])

    login(user, pwd, driver) # Not async because we need to login.

    acc_details.delay(driver, acc_id, acc_bal)
    print("Got acc details")
#    acc_result = acc_details.delay(driver, acc_id, acc_bal)
#    print(acc_result.result)

    trans_result = trans_info.delay(driver)
    print(trans_result.result)

    df_result = site2df.delay(acc_df, acc_id, acc_bal, beg_bal, tot_deb, tot_cred, end_bal)
    print(df_result.result)

    print(acc_df)

    driver.quit()


if __name__ == "__main__":
    print("Starting Scraper")
    main()